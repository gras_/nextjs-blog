import Head from 'next/head';
import Link from 'next/link';
import Date from '../components/date';
import Layout, {siteTitle} from '../components/layout';
import utilStyles from '../styles/utils.module.css';
import { getSortedPostsData } from '../lib/posts';
import { useTranslation } from 'next-i18next'
import { useRouter } from 'next/router';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import React from 'react';

const ClickThenRender = React.forwardRef(({ onClick, href }, ref) => {
  return (
    <a href={href} onClick={onClick} ref={ref} className={utilStyles.button}>
      查看文章列表
    </a>
  )
})

const Home = ({ allPostsData }) => {
  const router = useRouter()
  const {locale, defaultLocale, locales} = router
  const { t } = useTranslation()
  // console.log(allPostsData)
  return (
    <Layout home >
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <p>{t('introduce')}</p>
        <p>
          ({t('sample')} - you’ll be building a site like this on{' '}
          <a href="https://nextjs.org/learn">our Next.js tutorial</a>.)
        </p>
      </section>
      <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
        <h2 className={utilStyles.headingLg}>
          {t('blog')}
        </h2>
        <Link href="/click" locale={locale} passHref><ClickThenRender></ClickThenRender></Link>
        <ul className={utilStyles.list}>
          {allPostsData.map(({ id, date, title, locale }) => (
            <li className={utilStyles.listItem} key={id}>
              <Link href={`/posts/${id}`} locale={locale}>
                <a>{title}</a>
              </Link>
              <br />
              <small className={utilStyles.lightText}>
                <Date dateString={date} />
              </small>
            </li>
          ))}
        </ul>
      </section>
    </Layout>
  );
}

export async function getStaticProps({ locale }) {
  const allPostsData = getSortedPostsData();
  return {
    props: {
      allPostsData,
      ...(await serverSideTranslations(locale, ['common'])),
    },
  };
}

export default Home;
