import Layout from '../../components/layout';
import { getAllPostIds, getPostData } from '../../lib/posts';
import Head from 'next/head';
import Date from '../../components/date';
import utilStyles from '../../styles/utils.module.css';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next'
import { useRouter } from 'next/router';

export async function getStaticProps({ params, locale}) {
  const postData = await getPostData(params.id, locale); //params來自網址，id是因為檔名是[id].js

  return {
    props: {
      postData,
      ...(await serverSideTranslations(locale, ['common'])),
    },
  };
}

export async function getStaticPaths({ locales }) {
  let paths = []
  
  for(const locale of locales){
    let localePath = getAllPostIds(locale)
    paths = paths.concat(localePath)
  }
  console.log(paths)
  return {
    paths,
    fallback: false, // 若使用者造訪沒有在paths的頁面: false => 回傳404； true => https://ithelp.ithome.com.tw/articles/10269586
  }
}

export default function Post({ postData }) {
  const router = useRouter()
  const {locale} = router
  const { t } = useTranslation()
  // console.log(router, locale)
  return (
    <Layout>
      <Head>
        <title>{postData.title}</title>
      </Head>
      <article>
        <h1 className={utilStyles.headingXl}>{postData.title}</h1>
        <div className={utilStyles.lightText}>
          <Date dateString={postData.date} />
        </div>
        <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} />
      </article>
      <div>{t('back-to-home')}</div>
    </Layout>
  );
}