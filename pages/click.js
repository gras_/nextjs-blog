import React, {useState, useEffect} from 'react';
import Head from 'next/head';
import Link from 'next/link';
import Date from '../components/date';
import Layout, {siteTitle} from '../components/layout';
import utilStyles from '../styles/utils.module.css';
import { useTranslation } from 'next-i18next'
import { useRouter } from 'next/router';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';


export async function getStaticProps({ locale }) {
  let allData;
  await fetch(`https://jsonplaceholder.typicode.com/posts`)
    .then((res) => res.json())
    .then((data) => {
      allData = data
    })
  return {
    props: {
      locale,
      allData,
      ...(await serverSideTranslations(locale, ['common'])),
    },
  };
}

const Click = ({ allData }) => {
  const [data, setData] = useState(allData);
  const [author, setAuthor] = useState('All')
  const [isAll, setIsAll] = useState(true)

  const postList = (posts) => {
    if (!posts) return <p>No data</p>
    return (
      posts.map(({ id, title, body }) => (
        <li className={utilStyles.listItem} key={id}>
          
            <a>{title}</a>
          
          <br />
          <small className={utilStyles.lightText}>
            <Date dateString={'2022-05-10'} />
          </small>
        </li>
      ))
    )
  }

  const handleClick = (event) => {
    event.preventDefault();
    if(isAll){
      fetch(`https://jsonplaceholder.typicode.com/posts?userId=5`)
        .then((res) => res.json())
        .then((data) => {
          setData(data)
          setAuthor('Me')
          setIsAll(false)
        })
    }else{
      setData(allData)
      setAuthor('All')
      setIsAll(true)
    }
  }

  return (
    <Layout >
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <div onClick={handleClick} className={utilStyles.button}>
        {isAll ? '我的文章' : '所有文章'}
        </div>
      </section>
      <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
        <h2 className={utilStyles.headingLg}>
          文章列表 <small className={utilStyles.lightText}>{author}</small>
        </h2>
        <ul className={utilStyles.list}>{postList(data)}</ul>
      </section>
    </Layout>
  );
}

export default Click;